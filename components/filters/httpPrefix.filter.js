;(function() {

  'use strict';

  /**
   * Fitler which removes http/https protocol from string
   *
   * @author Jozef Butko
   * @ngdoc  Filter
   *
   * @example
   * {{ 'http://url.com' | httpPrefix }}
   *
   */
  angular
    .module('urlApp')
    .filter('httpPrefix', httpPrefix);


  function httpPrefix() {

    // Definition of filter
    return function (input) {
      // test string if it contains http or https prefix
      var hasPrefix = input.indexOf('http://') > -1 || input.indexOf('https://') > -1;
      return hasPrefix ? input.split('://')[1] : input;
    };
  }

})();