;(function() {

  'use strict';

  /**
   * External links directive
   *
   * @author Jozef Butko
   * @ngdoc  Directive
   *
   * @example
   * <external-links></external-links>
   *
   */
  angular
    .module('urlApp')
    .directive('externalLinks', externalLinks);


  function externalLinks() {

    // Definition of directive
    var directiveDefinitionObject = {
      restrict: 'E',
      templateUrl: 'views/external-links.html',
      controller: 'MainController',
      controllerAs: 'main'
    };

    return directiveDefinitionObject;
  }

})();