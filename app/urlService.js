;(function() {


  /**
   * Service for saving, deleting, fetching and selecting links
   *
   * @author Jozef Butko
   * @ngdoc  Factory
   *
   */
  angular
    .module('urlApp')
    .factory('urlService', urlService);


  urlService.$inject = ['$http', 'LocalStorage'];


  ////////////


  function urlService($http, LocalStorage) {

    // check localStorage if we have some links stored
    // else create empty array which will hold links
    var linksList = fetchLinks();

    // public API
    return {
      saveLink: saveLink,
      deleteLinks: deleteLinks,
      fetchLinks: fetchLinks,
      selectLinks: selectLinks,
      checkSelectLinks: checkSelectLinks
    };


    ////////////  function definitions

    /**
     * Save URL link
     *
     * @param  {string} url   URL link
     * @param  {string} title URL title
     * @return {array}  linkList Array with links
     */
    function saveLink(url, title) {

      linksList = fetchLinks();

      if (typeof url === 'undefined') {
        console.log('No link provided');
        return false;
      }

      // if we do not have URL title set it to URL
      if (typeof title === 'undefined' || title === '') {
        title = url;
      }

      // check if url contains http || https
      // if not add the http protocol
      var hasProtocol = url.substr(0,7) === 'http://' || url.substr(0,8) === 'https://';
      if (!hasProtocol) {
        url = 'http://' + url;
      }

      // setup link object
      var linkObj = {
        url: url,
        title: title,
        order: linksList.length + 1
      };

      // fake save link backend
      linksList.push(linkObj);

      // save link in localStorage === fake backend
      LocalStorage.update('links', linksList);

      return linksList;

      // following $http API call would be used in case real API
      //
      // return $http({
      //     method: 'POST',
      //     url: 'http://www.exampleapiurl.com/api/url',
      //     data: {
      //       url: url,
      //       title: title
      //     }
      //   }).then(function(data) {
      //     return data;
      //   })
      //   .catch(function(error) {
      //     return error;
      //   });
    }


    /**
     * Delete URL link from the list
     *
     * @return *
     */
    function deleteLinks() {

      // linksList = fetchLinks();

      linkListNew = linksList.filter(function (element, index, array) {
        return element.selected !== true;
      });
      console.log(linkListNew);
      console.log(linkListNew.length);
      LocalStorage.update('links', linkListNew);
      return linkListNew;

      // following $http API call would be used in case real API
      //
      // return $http({
      //     method: 'DELETE',
      //     url: 'http://www.exampleapiurl.com/api/url',
      //     data: {
      //       url: url
      //     }
      //   }).then(function(data) {
      //     return data;
      //   })
      //   .catch(function(error) {
      //     return error;
      //   });
    }


    /**
     * List all saved links
     *
     * @return {array}     Array with links
     */
    function fetchLinks() {
      // check localStorage if we have some links stored
      // else create empty array which will hold links
      return LocalStorage.get('links') === null ? [] : LocalStorage.get('links');

      // following $http API call would be used in case real API
      //
      // return $http({
      //   method: 'GET',
      //   url: 'http://www.exampleapiurl.com/api/url'
      // }).then(function(res) {
      //   console.log(res.data);
      // }, function(error) {
      //   console.log(error);
      // });
    }

    /**
     * Select link to be deleted
     *
     * @param  {string} url   URL link
     * @param  {string} title URL title
     */
    function selectLinks(url, title, order) {

      // fetch latest links array
      linksList = fetchLinks();

      var updatedLinksList = [];
      for (var i = 0; i < linksList.length; i++) {
        var link = linksList[i];

        if (link.url === url && link.title === title && link.order === order) {
          // toggle select attribute
          link.selected = link.selected === true ? false : true;
        } else {
          console.log('No match found');
        }
        updatedLinksList.push(link);
      }
      LocalStorage.update('links', updatedLinksList);
      return updatedLinksList;
    }

    /**
     * Check if any link from list is selected
     */
    function checkSelectLinks() {

      // fetch latest links array
      links = fetchLinks();

      if (links && links.length > 0) {
        var counter;
        for (var i = 0; i < links.length; i++) {
          var link = links[i];

          if (link.selected === true) {
            return true;
          }
        }
      }
    }

  }


})();
