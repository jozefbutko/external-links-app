;(function() {


  /**
   * Main application controller
   *
   * @author Jozef Butko
   * @ngdoc  Controller
   *
   */
  angular
    .module('urlApp')
    .controller('MainController', MainController);


  MainController.$inject = ['LocalStorage', 'urlService'];


  function MainController(LocalStorage, urlService) {

    // 'controller as' syntax
    var self = this;


    ////////////  function definitions


    /**
     * Save new link
     *
     * @param  {string} url   URL
     * @param  {string} title URL title
     * @param  {object} form  Form object from view
     */
    self.saveLink = function (url, title, form) {
      if (form.$valid) {
        self.links = urlService.saveLink(url, title);

        // clear input fields
        self.link = '';
        self.linkTitle = '';

        // check selected links
        self.anyLinkSelected = self.checkSelectedLinks();
      }
    };

    /**
     * Fetch links from API
     */
    self.fetchLinks = function () {
      self.links = urlService.fetchLinks();
    };

    /**
     * Delete link from the list
     */
    self.deleteLinks = function () {
      self.links = urlService.deleteLinks();

      // check selected links
      self.anyLinkSelected = self.checkSelectedLinks();
    };

    /**
     * Select links which will be deleted
     *
     * @param  {string} url   URL
     * @param  {string} title URL title
     * @param  {number} order List order value
     */
    self.selectLinks = function (url, title, order) {
      self.links = urlService.selectLinks(url, title, order);

      // check selected links
      self.anyLinkSelected = self.checkSelectedLinks();
    };


    /**
     * Check if any link from list is selected
     * @return {boolean} True/false
     */
    self.checkSelectedLinks = function() {
      return urlService.checkSelectLinks();
    };

    // check selected links on page load
    // helper for Remove link button for disabled state
    self.anyLinkSelected = self.checkSelectedLinks();

    // fetch links on page load
    self.fetchLinks();
  }


})();